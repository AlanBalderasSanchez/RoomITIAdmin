angular
.module('RDash')
.controller('AdministradoresCtrl', ['$scope','$location','$http','$document','loadAdministradores', AdministradoresCtrl]);

function AdministradoresCtrl($scope,$location,$http,$document,serviceAdmin) {

  //Main View

  $scope.dataAdministradores = [];


  $scope.dataAdministradores = serviceAdmin.loadAdministradores().then(function (result) {
    $scope.dataAdministradores = result.data.data;
  });


  $scope.editarAdmin = function(id){

  }

  //Agregar usuario

  $document.ready(function(){
      $('[data-toggle="tooltip"]').tooltip();
  });

  $scope.newAdmin ={
    nombre:"",
    aPaterno:"",
    aMaterno:"",
    username:"",
    password:"",
    cpassword:"",
    IFE:"/ife.png",
    compo_dom:"comp.png",
    direccion:{
      calle:"",
      colonia:"",
      cp:"",
      municipio:"",
      estado:"",
      numInt:"",
      numExt:""
    }
  };

  $scope.validaPass = function(){
    if($scope.newAdmin.password  != $scope.newAdmin.cpassword  &&  $scope.password.trim() != ""){
      return false;
    }
    return true;
  };

  //Función para agregar la clase de error a los grupos de label e input  y mostrar el mensaje de error.
  function addClassError(idDomElement){
    console.log($("#"+idDomElement).closest("form-group"));
    $("#"+idDomElement).closest(".form-group").addClass("has-error");
  }


  $scope.validacion=function(){
    var valido= true;

    /*
    Validación REGEXP para el nombre, únicamente caractéres alfabéticos con acentos.
    Considerando nombres comúnes en latinoamérica. No se consideran nombres comúnes
    provenientes de países bajos, o de origen Germánico en general
    */

    //Se permiten caractéres mayúsculas y minúsculas. Además de vocales minúsculas y mayúsculas con tílde.
    var regexp_nombres  = /^[a-zA-ZáéíóúÁÉÍÓÚÑñ\s]*$/;
    //Se perminte caractéres numéricos y alfabéticos
    var regexp_password = /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,}$/;
    //Similar a nombres pero podemos agregar dígitos
    var regexp_direccion_str = /^[a-zA-ZáéíóúÁÉÍÓÚÑñ\s0-9]*$/;
    //Se aceptan sólo dígitos
    var regexp_cp = /^([0-9])*$/;


    if ($scope.newAdmin.nombre.trim() == ""  ||  !regexp_nombre.test($scope.newAdmin.nombre.trim())){
      addClassError("nombre");
      valido = false;
    }

    if ($scope.newAdmin.aPaterno.trim() == ""  ||  regexp_nombres.test($scope.newAdmin.aPaterno.trim())){
      addClassError("a_paterno");
      valido = false;
    }

    if($scope.newAdmin.aMaterno.trim() == ""  ||  regexp_nombres.test($scope.newAdmin.aMaterno.trim())){
      addClassError("a_materno");
      valido = false;
    }

    //Buscar REGEXP para nombres de usuario, ya que pueden contener guines bajos, etc.
    if ($scope.newAdmin.username.trim() == ""){
      addClassError("username");
      valido = false;
    }

    //Verificar validéz de la contraseña
    if ($scope.newAdmin.password.trim()!="" &&  regexp_password.test($scope.newAdmin.password.trim())){
      addClassError("password");
      valido = false;
    }
    //Verificar que la contraseña coincida con la de validación.
    if ($scope.newAdmin.cpassword.trim() != $scope.newAdmin.password.trim()  ||  $scope.newAdmin.cpassword.trim() !="" ){
      addClassError("cpassword");
      valido = false;
    }

    //Falta validación de archivos via PHP
    if ($scope.newAdmin.IFE == ""){
      valido = false;
    }

    //Falta validación de archivos via PHP
    if ($scope.newAdmin.compo_dom == ""){
      valido = false;
    }

    /*
    Se valida la calle y colonia bajo un mismo regexp
    */
    if ($scope.newAdmin.direccion.calle.trim() == "" || !regexp_direccion_str.test($scope.newAdmin.direccion.calle.trim())){
      addClassError("calle");
      valido = false;
    }

    if ($scope.newAdmin.direccion.colonia.trim() == "" || !regexp_direccion_str.test($scope.newAdmin.direccion.colonia.trim())){
      addClassError("colonia");
      valido = false;
    }


    if ($scope.newAdmin.direccion.cp.trim() == "" || !regexp_cp.test($scope.newAdmin.direccion.cp.trim())){
      addClassError("cp");
      valido = false;
    }

    if ($scope.newAdmin.direccion.municipio.trim() == "" || !regexp_nombre.test($scope.newAdmin.direccion.municipio.trim())){
      addClassError("municipio");
      valido = false;
    }

    if($scope.newAdmin.direccion.estado.trim() == "" || !regexp_nombre.test($scope.newAdmin.direccion.estado.trim())){
      addClassError("estado");
      valido = false;
    }

    if ($scope.newAdmin.direccion.numInt.trim() == "" || regexp_direccion_str.test($scope.newAdmin.direccion.numInt.trim())){
      addClassError("num_int");
      valido = false;
    }

    if($scope.newAdmin.direccion.numExt.trim() == "" || regexp_direccion_str.test($scope.newAdmin.direccion.numExt.trim())){
      addClassError("num_ext");
      valido = false;
    }


    return valido;
    //directo si ya se probó que funciona el primer if return ($scope.newAdmin.nombre.trim()!=""||/^[a-zA-Z\s]*$/.test($scope.newAdmin.nombre.trim()));s
  }

  $scope.submitAdmin = function(){
    if($scope.validacion()){
      var req = {
        method:'POST',
        url:'php/addAdministrador.php',
        headers: {
          'Content-Type': undefined
        },
        data:{
          dataAdmin:JSON.stringify($scope.newAdmin)
        }
      };

      $http(req).then(function successCallback(response) {
        // this callback will be called asynchronously
        // when the response is available
        console.log(response.data);
        if(response.data == 'correcto')
        $location.path('administradores');

        else{
          alert("Hubo un error al intentar crear el registro");
          $location.path('/administradores');
        }

      }, function errorCallback(response) {
        console.log(response);
        // called asynchronously if an error occurs
        // or server returns response with an error status.
      });

    }else{
      alert("No se pudo guardar su informacion, algunos campos son incorrectos");
    }
  }


}
