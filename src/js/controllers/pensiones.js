angular
.module('RDash')
.controller('PensionesCtrl', ['$scope','$location','$document','$window','NgMap', PensionesCtrl]);

function PensionesCtrl($scope,$location,$document,$window,NgMap) {

  //id,nombre, precio, teléfono
  $scope.dataPension = [
    {"id":"1","nombre":"Paso de Luz","precio":"1200","telefono":"8912013"},
    {"id":"2","nombre":"Pensión Rio Papagayo","precio":"1000","telefono":"2011212"},
    {"id":"3","nombre":"Pensión Mal Aguero","precio":"1100","telefono":"2901212"},
    {"id":"4","nombre":"Pensión Sánchez","precio":"990","telefono":"9897164"},
    {"id":"5","nombre":"Pensión Sta María","precio":"1350","telefono":"8101212"}
  ];


  //Agregar PensionesCtrl
  $scope.newPension = {
    "nombre":"",
    "precio":"",
    "telefono":"",
    "caracteristicas":[],
    "servicios":[],
    "requisitos":"",
    "direccion":{
      "calle":"",
      "colonia":"",
      "cp":"",
      "municipio":"",
      "estado":"",
      "num_int":"",
      "num_ext":""
    }
  };

$scope.testAC = [
  "agua",
  "agua caliente",
  "refrigerador",
  "servicio 24 hrs"
];

  //La latitud y longitud por default estarán asignadas a la posición de Puebla
  $scope.lat = 19.0400572;
  $scope.ln  = -98.2630061;
  //Se formatea la posición como una cadena para que el mapa la pueda renderizar correctamente
  $scope.mapPos = '['+$scope.lat+','+$scope.ln+']';

  /*
    Damos el token de acceso para poder utilizar el API de google en modo productivo (en desarrollo se logra
    renderizar) aunque no esté asignado el token de acceso (key).
  */
  $scope.googleMapsUrl="https://maps.googleapis.com/maps/api/js?key=AIzaSyDN8l9MC1VA1SdaBEZ1tifCjnB74dN2bp4";

  /*
    Variable para saber si el mouse está dentro del mapa para posteriormente renderizar un menú contextual
    en caso de que se de click en el botón izquierdo del mouse y se la posición del mouse esté dentro del
    mapa.
  */
  var mouseInsideMap = false;
  //Manejo de las características del mapa desde aquí
  NgMap.getMap().then(function(map) {

  //Mouse dentro del área del mapa
   google.maps.event.addListener(map,"mouseover",function(evt){
     //Cambiamos la variable a verdadera, ya que está dentro del mapa.
      mouseInsideMap = true;
      console.log(mouseInsideMap);
   });

   //Mouse fuera del área del mapa
   google.maps.event.addListener(map,"mouseout",function(evt){
     //Cambiamos la variable a falso, ya que está fuera del mapa.
      mouseInsideMap = false;
      console.log(mouseInsideMap);
   });

   google.maps.event.addListener(map,"rightclick",function(evt){
     console.log("console menú :)");
   });

  });

  $scope.selected = 'None';
$scope.items = [
    { name: 'John', otherProperty: 'Foo' },
    { name: 'Joe', otherProperty: 'Bar' }
];

$scope.menuOptions = [
    ['Select', function ($itemScope, $event, modelValue, text, $li) {
        $scope.selected = $itemScope.item.name;
    }],
    null, // Dividier
    ['Remove', function ($itemScope, $event, modelValue, text, $li) {
        $scope.items.splice($itemScope.$index, 1);
    }]
];

}
