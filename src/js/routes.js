'use strict';

/**
 * Route configuration for the RDash module.
 */
angular.module('RDash').config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

        // For unmatched routes
        $urlRouterProvider.otherwise('/');

        // Application routes
        $stateProvider
            .state('index', {
                url: '/',
                templateUrl: 'templates/pensiones.html'
            })
            .state('tables', {
                url: '/tables',
                templateUrl: 'templates/tables.html'
            })
            .state('login', {
                url: '/login',
                templateUrl: 'templates/login.html'
            })
            .state('pensiones', {
                url: '/pensiones',
                templateUrl: 'templates/pensiones.html'
            })
            .state('agregarPension',{
                url:'/pensiones/agregar',
                templateUrl:'templates/agregarPension.html'
            })
            .state('administradores', {
                url: '/administradores',
                templateUrl: 'templates/administradores.html'
            })
            .state('agregarAdministrador',{
                url:'/administradores/agregar',
                templateUrl:'templates/agregarAdministrador.html'
            })

            ;
    }
]);
