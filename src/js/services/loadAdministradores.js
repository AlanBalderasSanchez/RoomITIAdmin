angular
.module('RDash')
.service('loadAdministradores', ['$q','$timeout','$http', loadAdministradores]);

function loadAdministradores ($q,$timeout,$http){

  var loadAdministradores =
  function(){

   	var deferred = $q.defer();
    var req = {
      method:'POST',
      url:'php/loadAdministradores.php',
      headers: {
        'Content-Type': undefined
      }
    };

    $http(req).then(
      function successCallback(response) {
        $timeout(function(){
            deferred.resolve({
              data: response
            });
          });
      },
      function errorCallback(response) {
            $timeout(function(){
            deferred.resolve({
              data: response
            });
          });
      });

      return deferred.promise;
    };


    return {
      loadAdministradores:loadAdministradores
    };

  }
